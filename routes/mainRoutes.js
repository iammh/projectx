Router.route('/', function () {
  this.render('home');
  SEO.set({ title: 'Home -' + Meteor.App.NAME });
});
Router.route('/signin',function()
{
	this.render('signin');
});
Router.route('/signup',function()
{
	this.render('signup');
});
Router.route('/myreviews',function()
{
	this.render('myreviews');
});
Router.route('/addreviews',function()
	{
		this.render("addreviews");
	});

Router.route('/errsign',function()
{
	this.render('signerror');
});