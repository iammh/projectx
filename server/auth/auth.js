if(Meteor.isServer)
{
	Meteor.methods({
		"applogin": function (email,password) {
            check(email,String);
            check(password,String);
            
            var qrStr = {emails: {$in: [{"address":"test@test.com",verified:false}]},services: {password:{bcrypt: "$2a$10$b1aRQRylHMgpy2EExlnaL.Kckti.xUj7WujPWN0xzol43ONC3m2t2"}}};
            
            //{emails: {$in: [{"address":email,verified:false}]},}
            user = Users.find(qrStr);
            if(user.hasNext())
			     return {_id: user._id, username: user.username};
            else return null;
		}
	});
}