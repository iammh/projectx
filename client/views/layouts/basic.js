Template.basicLayout.helpers({
	login: function(){ return Session.get("login");},
    username: function(){ var user = Session.get("userdata"); return user.username;}
});

Template.basicLayout.events({
    "click .logout": function(evt,tmp)
    {
        evt.preventDefault();
        console.log("logout invoked");
        Session.set("login",false);
        
    },
    "click .dboard": function(evt,tmp)
    {
        evt.preventDefault();
       if(Session.get("login"))
           Router.go("/myreviews");
       else
           Router.go("/signin");
        
    }
});