Template.addreviews.helpers({
	doadd: function(){
		return Session.get("addclick")? true:false;
	}
});

Template.addreviews.events({
	"click .add": function(ev,rv)
	{
		ev.preventDefault();
		Session.set("addclick",true);
	},
	"click .can": function(ev,rv)
	{
		ev.preventDefault();
		Session.set("addclick",false);
	},
	"click .sub": function(ev,rv)
	{
		ev.preventDefault();
	}
});